import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localHost:8000'
});

export default instance;