import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class MessageForm extends Component {
  state = {
    author: '',
    message: '',
    image: '',
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);

    this.setState({author: '', message: '', image: ''});
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="Author">
          <Col componentClass={ControlLabel} sm={2}>
            Author
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              placeholder="Author's name"
              name="author"
              value={this.state.author}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
        <FormGroup controlId="Message">
          <Col componentClass={ControlLabel} sm={2}>
            Message
          </Col>
          <Col sm={10}>
            <FormControl
              required
              componentClass="textarea"
              placeholder="Enter message"
              name="message"
              value={this.state.message}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
        <FormGroup controlId="productImage">
          <Col componentClass={ControlLabel} sm={2}>
            Image
          </Col>
          <Col sm={10}>
            <FormControl
              type='file'
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default MessageForm;
