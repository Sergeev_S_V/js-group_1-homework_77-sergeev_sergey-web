import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = () => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
      <LinkContainer to="/" exact><a>Chat</a></LinkContainer>
      </Navbar.Brand>
    </Navbar.Header>
    <Nav pullRight>
      <LinkContainer to="/" exact>
        <NavItem>Messages</NavItem>
      </LinkContainer>
    </Nav>
  </Navbar>
);

export default Toolbar;
