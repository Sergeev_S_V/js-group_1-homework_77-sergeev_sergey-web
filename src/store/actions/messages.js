import axios from '../../axios-api';

import {CREATE_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS} from "./actionTypes";

export const fetchMessagesSuccess = messages => {
  return {type: FETCH_MESSAGES_SUCCESS, messages};
};

export const fetchMessages = () => dispatch => {
  axios.get('/messages')
    .then(resp => dispatch(fetchMessagesSuccess(resp.data)));
};

export const createMessageSuccess = message => {
  return {type: CREATE_MESSAGE_SUCCESS, message};
};

export const createMessage = messageData => dispatch => {
  return axios.post('/messages', messageData)
    .then(resp => resp ? dispatch(createMessageSuccess(resp.data)) : null)
};