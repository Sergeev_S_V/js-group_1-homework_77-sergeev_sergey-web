import React, {Component, Fragment} from 'react';
import MessageForm from '../../Components/MessageForm/MessageForm';
import {connect} from "react-redux";
import {createMessage} from "../../store/actions/messages";

class NewMessage extends Component {

  createMessage = messageData => {
    this.props.onMessageCreated(messageData)
      .then(() => this.props.clicked());
  };

  render() {
    return (
      <Fragment>
        <h2>New message</h2>
        <MessageForm onSubmit={this.createMessage} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onMessageCreated: messageData => dispatch(createMessage(messageData))
  }
};

export default connect(null, mapDispatchToProps)(NewMessage);
