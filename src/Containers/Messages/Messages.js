import React, {Component, Fragment} from 'react';
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import {connect} from "react-redux";

import {fetchMessages} from "../../store/actions/messages";
import Modal from "../../Components/UI/Modal/Modal";
import NewMessage from "../NewMessage/NewMessage";
import './Messages.css';

class Messages extends Component {

  state = {
    showModal: false,
  };

  componentDidMount() {
    this.props.onFetchMessages();
  };

  toggleModalHandler = () => {
    this.setState(prevState => ({showModal: !prevState.showModal}));
  };

  render() {
    return(
      <Fragment>
        <Modal show={this.state.showModal} closed={this.toggleModalHandler}>
          <NewMessage clicked={this.toggleModalHandler}/>
        </Modal>
        <PageHeader>
          Messages
          <Button onClick={this.toggleModalHandler} className='pull-right' bsStyle='primary'>
            Add message
          </Button>
        </PageHeader>
        {this.props.messages.map(message => (
          <Panel key={message.id} className='Message-Form'>
            <Panel.Body >
              <span className='Message-Text'>
                <strong className='Message-Color'>
                author: {message.author}
                </strong>
              </span>
              <p className='Message-Message' style={{width: message.image ? '60%' : '100%'}}>
                {message.message}
              </p>
              {message.image && <Image
                src={'http://localhost:8000/uploads/' + message.image}
                thumbnail
                style={{width: '32%', height: '32%', float: 'right'}}
              />}
            </Panel.Body>
          </Panel>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchMessages: () => dispatch(fetchMessages())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);