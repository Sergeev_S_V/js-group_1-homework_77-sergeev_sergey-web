import React, { Component } from 'react';
import Toolbar from "./Components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";
import Messages from "./Containers/Messages/Messages";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <header>
          <Toolbar/>
        </header>
        <main className="container">
          <Switch>
            <Route path='/' exact component={Messages}/>
          </Switch>
        </main>
      </div>
    );
  }
}

export default App;
